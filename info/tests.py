from django.test import LiveServerTestCase, TestCase, Client
from django.urls import resolve
from .views import *

# javascript ga usah dites karena merupakan client-side-server
class Test(TestCase):
	def test_ada_url_kosong(self):
		c = Client()
		response = c.get('//')
		self.assertEqual(response.status_code, 200)

	def test_halaman_utama_menggunakan_index_html(self):
		c = Client()
		response = c.get('//')
		self.assertTemplateUsed(response, 'index.html')

	def test_halaman_utama_pakai_fungsi_index(self):
		function = resolve('/')
		self.assertEqual(function.func, index)

	def test_include_jquery(self):
		c = Client()
		response = c.get('//')
		content = response.content.decode('utf8')
		self.assertIn("https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js", content)

	def test_ada_script(self):
		c = Client()
		response = c.get('//')
		content = response.content.decode('utf8')
		self.assertIn("<script", content)

	def test_ada_button_light(self):
		c = Client()
		response = c.get('//')
		content = response.content.decode('utf8')
		self.assertIn("<button", content)
		self.assertIn("Light", content)

	def test_ada_button_dark(self):
		c = Client()
		response = c.get('//')
		content = response.content.decode('utf8')
		self.assertIn("<button", content)
		self.assertIn("Dark", content)


from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class Story7FunctionalTest(LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Story7FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Story7FunctionalTest, self).tearDown()

	def test_klik_tombol_dan_accordion(self):
		selenium = self.selenium
		# buka link yg mau dites
		selenium.get(self.live_server_url)

		# cari elemen
		dark = selenium.find_element_by_id('dark')
		light = selenium.find_element_by_id('light')
		aktivitas = selenium.find_element_by_id('ui-id-1')
		pengalaman = selenium.find_element_by_id('ui-id-3')
		prestasi = selenium.find_element_by_id('ui-id-5')

		# klik tombol tema dark
		dark.send_keys(Keys.RETURN)

		# klik tombol tema light
		light.send_keys(Keys.RETURN)

		# expand accordion
		aktivitas.send_keys(Keys.RETURN)
		pengalaman.send_keys(Keys.RETURN)
		prestasi.send_keys(Keys.RETURN)
