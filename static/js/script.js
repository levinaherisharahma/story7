$(document).ready(function(){
  $("#light").click(function(){
    $("html").css({"background": "white"});
    $("body").css({"background": "white"});
    $("h3").css({"color": "black"});
    $(".container").css({"background": "white"});
    $(".outline").css({"border": "1.5px solid black"});
    $(".title").css({"background": "#ff5e5e", "color": "white"})
    // $(".ui-accordion-header.ui-state-active").css({"background": "#3eb5c1", "border": "#27bf79"})
    $(".ui-widget-content").css({"background": "white", "border": "#dddddd", "color": "black"})
  });

  $("#dark").click(function(){
    $("html").css({"background": "#303d57"});
    $("body").css({"background": "#303d57"});
    $("h3").css({"color": "white"});
    $(".container").css({"background": "#303d57"});
    $(".outline").css({"border": "1.5px solid white"});
    $(".title").css({"background": "#fca227", "color": "black"})
    // $(".ui-accordion-header.ui-state-active").css({"background": "#FC814A", "border": "#FC814A"})
    $(".ui-widget-content").css({"background": "#2b3444", "border": "#2b3444", "color": "white"})
  });

  $('#accordion').accordion({		//biar pas loading pertama ketutup semua
    active: false,
    collapsible: true            
	}); 

  $( function() {
    $("#accordion").accordion();
  });
});
